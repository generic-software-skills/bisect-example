git bisect example
==================

This is an example code of implementing arbitrary-precision integers with a class called `MyInt`
and has (at least) one bug in it. So please view it as an an exercise using `git bisect` -
not as a reference implementation in any way!
(There are many ways to do a better job with that)
