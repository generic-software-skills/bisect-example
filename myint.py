def String_Digit_Add_Sub(str_digit_a, str_digit_b, carryover=False, direction=1):
    """carryover is True if a call for the previous position influenced this position.
    direction is +1 for addition and -1 for subtraction.
    """
    if (direction == 1):
        result = int(str_digit_a) + int(str_digit_b)
    else:
        result = int(str_digit_a) - int(str_digit_b)

    if (carryover):
        result += direction

    next_carryover = False
    if ((direction == 1) and (result > 9)) or ((direction == -1) and (result < 0)):
        result += -direction*10
        next_carryover = True

    return [next_carryover, str(result)]


def String_Digit_Mult(str_digit_a, str_digit_b, carryover=0):
    result = int(str_digit_a)*int(str_digit_b) + carryover
    return [ result // 10, str(result % 10) ]


def String_Digitline_Mult(str_digit_a, str_digitline):
    carryover = 0
    result = ''
    for idx in range(len(str_digitline)):
        carryover, temp_result = String_Digit_Mult(str_digit_a, str_digitline[-idx-1], carryover)
        result = temp_result + result

    if (carryover != 0):
        result = str(carryover) + result

    return result


def Remove_Leading_Zeros(number):
    cut_off_idx = 0
    for idx in range(len(number)):
        if (number[idx] == '0'):
            cut_off_idx = idx+1
        else:
            break

    result = number[cut_off_idx:]
    if (result == ''):
        result = '0'

    return result


class MyInt():
    def __init__(self, string_value):
        self.string_value = string_value
        if (self.string_value.startswith('+')):
            self.string_value = self.string_value[1:]

        self.negative = False
        if (self.string_value.startswith('-')):
            self.string_value = self.string_value[1:]
            self.negative = True

        self.string_value = Remove_Leading_Zeros(self.string_value)

    def __str__(self):
        if (self.negative):
            return '-' + self.string_value
        else:
            return self.string_value

    def __le__(self, other):
        if (self.is_negative() == other.is_negative()):
            self_string = self.get_value()
            other_string = other.get_value()
            if (len(self.string_value) < len(other_string)):
                return False if self.is_negative() else True
            elif (len(self.string_value) == len(other_string)):
                idx = 0
                while True:
                    if (int(self.string_value[idx]) < int(other_string[idx])):
                        return False if self.is_negative() else True
                    if (int(self.string_value[idx]) > int(other_string[idx])):
                        return True if self.is_negative() else False
                    elif (idx == len(self.string_value)-1):
                        return True

                    idx += 1

            return True if self.is_negative() else False
        else:
            if (self.is_negative()):
                return True
            else:
                return False

    def __gt__(self, other):
        return not self.__le__(other)

    def _String_Add_Sub(self, other, direction=1):
        self_string = self.get_value()
        other_string = other.get_value()

        result = ''
        min_len = min(self.num_digits(), other.num_digits())
        carryover = False
        for idx in range(min_len):
            carryover, temp_str_digit = String_Digit_Add_Sub(self_string[-idx-1], other_string[-idx-1], carryover, direction)
            result = temp_str_digit + result

        if (self.num_digits() > min_len):
            for idx in range(min_len, self.num_digits()):
                carryover, temp_str_digit = String_Digit_Add_Sub(self_string[-idx-1], '0', carryover, direction)
                result = temp_str_digit + result

        if (other.num_digits() > min_len):
            for idx in range(min_len, other.num_digits()):
                carryover, temp_str_digit = String_Digit_Add_Sub('0', other_string[-idx-1], carryover, direction)
                result = temp_str_digit + result

        return [carryover, result]

    def __add__(self, other):
        self_string = self.get_value()
        other_string = other.get_value()
        if (self_string.startswith('-')) and (not other_string.startswith('-')):
            temp = MyInt(self_string[1:])
            return other.__sub__(temp)

        if (not self_string.startswith('-')) and (other_string.startswith('-')):
            temp = MyInt(other_string[1:])
            return self.__sub__(temp)

        if (self_string.startswith('-')) and (other_string.startswith('-')):
            temp_self = MyInt(self_string[1:])
            temp_other = MyInt(other_string[1:])
            abs_result = temp.__add__(other)
            return MyInt('-' + abs_result.get_value())

        carryover, result = self._String_Add_Sub(other, direction=1)
        if (carryover):
            result = '1' + result

        return MyInt(result)

    def __sub__(self, other):
        self_string = self.get_value()
        other_string = other.get_value()
        if (not self_string.startswith('-')) and (other_string.startswith('-')):
            temp = MyInt(other_string[1:])
            return self.__add__(temp)

        if (self_string.startswith('-')) and (not other_string.startswith('-')):
            temp = MyInt(self_string[1:])
            abs_result = temp.__add__(other)
            return MyInt('-' + abs_result.get_value())

        if (self_string.startswith('-')) and (other_string.startswith('-')):
            temp_self = MyInt(self_string[1:])
            temp_other = MyInt(other_string[1:])
            return temp_other.__sub__(temp_self)

        if (self < other):
            temp = other - self
            return MyInt('-' + temp.get_value())

        carryover, result = self._String_Add_Sub(other, direction=-1)
        return MyInt(result)

    def __mul__(self, other):
        if (self > other):
            return other.__mul__(self)

        num_negative = 0
        self_string = self.get_value()
        if (self.is_negative()):
            self_string = self_string[1:]
            num_negative += 1

        other_string = other.get_value()
        if (other.is_negative()):
            other_string = other_string[1:]
            num_negative += 1

        result = MyInt('0')
        for idx in range(len(self_string)):
            temp_result = MyInt(String_Digitline_Mult(self_string[-idx-1], other_string))
            shifted_result = temp_result.get_value() + ''.join(['0' for x in range(idx)])
            result += MyInt(shifted_result)

        if (num_negative == 1):
            result = MyInt('-' + result.get_value())

        return result

    def num_digits(self):
        return len(self.string_value)

    def is_negative(self):
        return self.negative

    def get_value(self):
        return self.__str__()
