from myint import MyInt

testcases = [
    ('1', '5'),
    ('5', '4'),
    ('0', '2'),
    ('7', '0'),
    ('0', '0'),
    ('5', '9'),
    ('0', '10'),
    ('15', '9'),
    ('0', '+3'),
    ('+5', '+2'),
    ('106', '87'),
    ('179', '137'),
    ('79', '137'),
    ('01', '007'),
    ('-1', '2'),
    ('1', '-2'),
    ('-11829845', '89037'),
    ('271828182845904523536028747135266249', '314159265358979323846264338327950288'),
]

for firstval, secondval in testcases:
    a = MyInt(firstval)
    b = MyInt(secondval)
    c = a + b
    d = a - b
    e = b - a

    status = (a <= b)
    reference = (int(a.get_value()) <= int(b.get_value()))
    assert (status == reference), 'Comparison failed: Got ' + str(status) + ' instead of ' + str(reference)

    c_int = int(a.get_value()) + int(b.get_value())
    assert (int(c.get_value()) == c_int), 'Addition failed: Got ' + str(c) + ' instead of ' + str(c_int)
    print(str(a) + ' + ' + str(b) + ' = ' + str(c))

    d_int = int(a.get_value()) - int(b.get_value())
    assert (int(c.get_value()) == d_int), 'Subtraction (1) failed: Got ' + str(d) + ' instead of ' + str(d_int)
    print(str(a) + ' - ' + str(b) + ' = ' + str(d))

    e_int = int(b.get_value()) - int(a.get_value())
    assert (int(e.get_value()) == e_int), 'Subtraction (2) failed: Got ' + str(e) + ' instead of ' + str(e_int)
    print(str(b) + ' - ' + str(a) + ' = ' + str(e))

    c = a * b
    c_int = int(a.get_value()) * int(b.get_value())
    assert (int(c.get_value()) == c_int), 'Multiplication failed: Got ' + str(c) + ' instead of ' + str(c_int)
    print(str(a) + ' * ' + str(b) + ' = ' + str(c))
